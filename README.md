# 자율주행차 도로주행 적합성 평가 모듈



## 소개

멀티에이전트 자율주행 시뮬레이터에서 구현된 자율주행차의 운행 데이터를 활용하여 자율주행차 도로주행 적합성을 평가할 수 있는 모듈입니다.


## 담당기관

- 차세대융합기술연구원


## 데이터셋 상세 구성

- 독립변수: 도로구간별 곡선반경, 경사도, 도로구간 길이, 차로 수, 교차로 여부
1. 곡선반경: GPS 좌표 활용 산출 [(Ref. Jagelčák et al., 2022)](https://www.mdpi.com/1424-8220/22/6/2298)
2. 경사도: 도로구간의 시작점과 종점의 고도값 차이 활용
3. 곡선반경 변화량: 이전 도로구간의 곡선반경과 이후 도로구간의 곡선반경 차이 활용
4. 경사도 변화량: 이전 도로구간 경사도와 이후 도로구간 경사도 차이 활용
- 종속변수: 해당 도로구간 자율주행차 위험운전행동 여부(위험운전행동 탐지 모듈 활용)

종속변수 생성을 위한 모델 관련 정보는 하기 링크에서 확인 가능

https://gitlab.com/korea_addi/multi-agent-simulator/aict_driving-pattern-and-risky-driving

## Data 상세

- 자율주행차 운행 데이터: 판타G버스 기본안전메시지(Basic Safety Message; BSM)
- 도로구간 요인 데이터: 판교 자율주행차 시범운행지구 정밀도로지도

운행 데이터의 세부현황은 하기 링크에서 확인 가능

https://www.bigdata-geo.kr/user/dataset/view.do?data_sn=17

정밀도로지도 관련 정보는 하기 링크에서 확인 가능

https://map.ngii.go.kr/mn/mainPage.do

## 구동 프로그램

- Main: Python(독립변수)
- Sub: Rstudio + R(종속변수)

## 필수 Package

- 모델 구축 및 분석 Package(Python): Numpy, Pandas, xgboost
- 등급화 Package(R): dplyr, stringr, rJava
