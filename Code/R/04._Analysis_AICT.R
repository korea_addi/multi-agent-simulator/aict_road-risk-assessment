rm(list=ls())

load("Order2_preprocessing_AICT.Rdata")
load("Order3_clustering_AICT.Rdata")



pkg <- c("dplyr", "stringr", "rJava", "reshape", "jmotif", "seewave","Rtsne", "ggplot2")
lapply(pkg, install.packages, character.only = TRUE)
lapply(pkg, require, character.only = TRUE)
rm(pkg)


original <- as.data.frame(som_model$data)
node_vector <- as.data.frame(som_model$codes)
winning_neuron <- som_model$unit.classif

final_dataset <- all_PAA7_train
final_dataset$neuron <- winning_neuron
cluster = cl_tmp7$cluster[som_model$unit.classif]
final_dataset <- data.frame(final_dataset, cluster)

# write.csv(final_dataset, "final_dataset_AICT.csv")

# ====================================================================================================
### Record the average values for all variables in each cluster
# ====================================================================================================

avg_sp <- c()
for (i in 1:7){
  tmp_sp_av <- c(mean(final_dataset$sp1[final_dataset$cluster==i]),mean(final_dataset$sp2[final_dataset$cluster==i]),
                 mean(final_dataset$sp3[final_dataset$cluster==i]),mean(final_dataset$sp4[final_dataset$cluster==i]),
                 mean(final_dataset$sp5[final_dataset$cluster==i]),mean(final_dataset$sp6[final_dataset$cluster==i]),
                 mean(final_dataset$sp7[final_dataset$cluster==i]))
  avg_sp <- c(avg_sp,mean(tmp_sp_av))
}

avg_df_sp <- c()
for (i in 1:7){
  tmp_df_sp_av <- c(mean(final_dataset$df_sp1[final_dataset$cluster==i]),mean(final_dataset$df_sp2[final_dataset$cluster==i]),
                 mean(final_dataset$df_sp3[final_dataset$cluster==i]),mean(final_dataset$df_sp4[final_dataset$cluster==i]),
                 mean(final_dataset$df_sp5[final_dataset$cluster==i]),mean(final_dataset$df_sp6[final_dataset$cluster==i]),
                 mean(final_dataset$df_sp7[final_dataset$cluster==i]))
  avg_df_sp <- c(avg_df_sp,mean(tmp_df_sp_av))
}

avg_yaw <- c()
for (i in 1:7){
  tmp_yaw_av <- c(mean(final_dataset$yaw_rate1[final_dataset$cluster==i]),mean(final_dataset$yaw_rate2[final_dataset$cluster==i]),
                    mean(final_dataset$yaw_rate3[final_dataset$cluster==i]),mean(final_dataset$yaw_rate4[final_dataset$cluster==i]),
                    mean(final_dataset$yaw_rate5[final_dataset$cluster==i]),mean(final_dataset$yaw_rate6[final_dataset$cluster==i]),
                    mean(final_dataset$yaw_rate7[final_dataset$cluster==i]))
  avg_yaw <- c(avg_yaw,mean(tmp_yaw_av))
}

# Create a dataframe with summary info
cluster_summary <- as.data.frame(avg_sp)
cluster_summary <- cbind(cluster_summary, avg_df_sp)
cluster_summary <- cbind(cluster_summary, avg_yaw) 
#write.csv(cluster_summary, "cluster_summary.csv")


# Define Risky Driving Behavior Criteria based on variables(e.g. acceleration, yaw rate)
sp_crit = quantile(cluster_summary$avg_sp, probs=c(0.05, 0.95))
acc_crit = quantile(cluster_summary$avg_df_sp, probs=c(0.05, 0.95))
df_crit = data.frame(sp_crit, acc_crit)
yaw_crit = quantile(cluster_summary$avg_yaw, probs=c(0.05, 0.95))
df_crit = data.frame(df_crit, yaw_crit)

# Apply Risky Driving Behavior Criteria to each cluster to determine risky driving
for(i in 1:length(cluster_summary)){
  tmp_vec = c()
  for(j in 1:nrow(cluster_summary)){
    tmp_vec = c(tmp_vec, cluster_summary[j,i] < df_crit[1,i] | cluster_summary[j,i] > df_crit[2,i])
    tmp_df = as.data.frame(tmp_vec)
  }
  if(i==1){
    final_df = tmp_df
  }
  else{
    final_df = data.frame(final_df, tmp_df)
  }
}


# change the name of columns for defining risky driving clusters
names(final_df)[names(final_df)=='tmp_vec'] <- 'Speed'
names(final_df)[names(final_df)=='tmp_vec.1'] <- 'Acceleration'
names(final_df)[names(final_df)=='tmp_vec.2'] <- 'Yaw Rate'

# change the name of columns for defining risky driving clusters
risky_sp_cluster = which(final_df[1]==TRUE)
risky_acc_cluster = which(final_df[2]==TRUE)
risky_yaw_cluster = which(final_df[3]==TRUE)

risky_sp_cluster; risky_acc_cluster; risky_yaw_cluster



# Prediction
# Create test dataset
dataset <- norm_all_PAA7_test1
dataset_mat_test1 <- as.matrix(dataset)

dataset <- norm_all_PAA7_test2
dataset_mat_test2 <- as.matrix(dataset)

# Predict with the learned model
result_test1 <- predict(som_model, newdata=dataset_mat_test1)$unit.classif
result_test2 <- predict(som_model, newdata=dataset_mat_test2)$unit.classif

final_dataset_test1 <- all_PAA7_test1
final_dataset_test2 <- all_PAA7_test2

final_dataset_test1$neuron <- result_test1
final_dataset_test2$neuron <- result_test2

cluster_test1 = cl_tmp7$cluster[result_test1]
cluster_test2 = cl_tmp7$cluster[result_test2]

final_dataset_test1 <- data.frame(final_dataset_test1, cluster_test1)
final_dataset_test2 <- data.frame(final_dataset_test2, cluster_test2)


# Calculate road sections in risky driving clusters
road_train <- unique(final_dataset$road_sctn_id)
road_test1 <- unique(final_dataset_test1$road_sctn_id)
road_test2 <- unique(final_dataset_test2$road_sctn_id)

# Calculate risky driving index
risky_vec = c()
for(i in 1:length(road_train)){
  risky_vec = c(risky_vec, (nrow(final_dataset[(final_dataset$cluster==1) | (final_dataset$cluster==2) | (final_dataset$cluster==5),] 
                                 %>% filter(road_sctn_id==road_train[i]))) / (nrow(final_dataset%>% filter(road_sctn_id==road_train[i]))))
}
risky_train_df = data.frame(road_train, risky_vec)

risky_vec = c()
for(i in 1:length(road_test1)){
  risky_vec = c(risky_vec, (nrow(final_dataset_test1[(final_dataset_test1$cluster==1) | (final_dataset_test1$cluster==2) | (final_dataset_test1$cluster==5),] 
                                 %>% filter(road_sctn_id==road_test1[i]))) / (nrow(final_dataset_test1%>% filter(road_sctn_id==road_test1[i]))))
}
risky_test1_df = data.frame(road_test1, risky_vec)

risky_vec = c()
for(i in 1:length(road_test2)){
  risky_vec = c(risky_vec, (nrow(final_dataset_test2[(final_dataset_test2$cluster==1) | (final_dataset_test2$cluster==2) | (final_dataset_test2$cluster==5),] 
                                 %>% filter(road_sctn_id==road_test2[i]))) / (nrow(final_dataset_test2%>% filter(road_sctn_id==road_test2[i]))))
}
risky_test2_df = data.frame(road_test2, risky_vec)

# Compare risky driving index
names(risky_train_df)[names(risky_train_df)=='road_train'] <- 'road_sctn_id'
names(risky_test1_df)[names(risky_test1_df)=='road_test1'] <- 'road_sctn_id'
names(risky_test2_df)[names(risky_test2_df)=='road_test2'] <- 'road_sctn_id'

risky_test1_df_final <- inner_join(risky_train_df,risky_test1_df,by='road_sctn_id')
colnames(risky_test1_df_final)
risky_test1_df_final$suitability <- risky_test1_df_final$risky_vec.x >= risky_test1_df_final$risky_vec.y

risky_test2_df_final <- inner_join(risky_train_df,risky_test2_df,by='road_sctn_id')
risky_test2_df_final$suitability <- risky_test2_df_final$risky_vec.x >= risky_test2_df_final$risky_vec.y


